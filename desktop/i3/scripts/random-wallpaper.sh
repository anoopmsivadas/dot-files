#!/usr/bin/env sh

while [ 1=1 ]; do
    find -L ~/Pictures/Wallpapers -type f \( -name '*.jpg' -o -name '*.png' \) -size +500k -print0 | shuf -n1 -z | xargs -0 feh --bg-scale
    sleep 500
done
