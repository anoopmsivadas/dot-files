#!/usr/bin/env bash

if [ $(xrandr | grep -o "HDMI-1")=="HDMI-1" ];then
  echo 'HDMI monitor found - setting up monitor output'
  xrandr --output HDMI-1 --mode 1920x1080
else
  echo 'No HDMI monitor found'
fi
